import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer res = null;
        for(Integer nb : liste){
            if(res == null || res.compareTo(nb)>0){ //vérifie un à un les éléments de la liste et garde le plus petit des éléments parcourues
                res = nb;
            }
        }
        return res;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean res = true;
        for(T elem : liste){
            if(valeur.compareTo(elem)>=0){ // verifie un à un tout les éléments de la liste est si un d'eux est plus petit que la valeur à vérifier alors res prend la valeur false
                res = false;
            }
        }
        return res;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> res = new ArrayList<>();
        int taille = liste2.size(); // permet d'éviter à refaire un appel pour connaitre la taille de la liste
        for(T elem : liste1){
            if(!res.contains(elem)){ // vérifie que l'élément est pas déjà dans la réponse
                boolean fin = false;
                int ind = 0;
                while(!fin){
                    if(ind < taille){   
                        int val = elem.compareTo(liste2.get(ind));
                        if(val <= 0){ 
                            fin = true;// sachant que les deux listes sont trié , si l'élément de la liste 1 est plus petit que celui de la liste 2 , ce n'est pas la peine de poursuivre car ca sera le cas avec le reste des éléments de la liste 2
                            if(val == 0){ // si les deux éléments sont egaux
                                res.add(elem);
                            }
                        }
                        ind += 1;
                    }
                }
            }    
        }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        int taille = texte.length();
        int ind = 0;
        String mot = "";
        List<String> res = new ArrayList<>();
        while(ind < taille){
            if(texte.charAt(ind) != ' '){
                mot += texte.charAt(ind); // si le caractère n'est pas un espace , alors on l'ajoute au mot en cours de construction
            }
            else{
                if(! mot.equals("")){ // on ajoute le mot à la liste de mot si celui ci n'est pas vide est que le caractèe lu est un espace
                    res.add(mot);
                    mot = "";
                }
            }
            ind += 1;
        }
        if(! mot.equals("")){
            res.add(mot);
        }
        return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        List<String> listeMot = decoupe(texte);
        HashMap<String,Integer> cptParMot = new HashMap<>();
        for(String mot : listeMot){ // dans le dictionnaire se trouvera en clé les mots contenu dans la liste et en valeur de ces clé , le nombre de fois qu'ells apparaissent dans la liste
            if(cptParMot.containsKey(mot)){
                cptParMot.put(mot,cptParMot.get(mot)+1);
            }
            else{
                cptParMot.put(mot,1);
            }
        }
        String max = null;
        for(String cle : cptParMot.keySet()){ // recherche la clé avec la plus grande valeur
            if(max == null || cptParMot.get(max).compareTo(cptParMot.get(cle)) <= 0){
                if(max != null && cptParMot.get(max).compareTo(cptParMot.get(cle)) == 0){
                    if(max.compareTo(cle)>0){
                        max = cle;
                    }
                }
                else{
                    max = cle;
                }
            }
        }
        return max;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cpt = 0;
        int ind = 0;
        while(ind < chaine.length()){
            if(chaine.charAt(ind)=='('){
                cpt += 1;
            }
            else{
                if(chaine.charAt(ind)==')'){
                    cpt -= 1;
                }
            }
            if(cpt < 0){
                return false;
            }
            ind += 1;
        }
        return cpt == 0;// le compteur doit être à 0 si le texte est bien parenthésé car on ajoute un à chaque ( et on enlève un à chaque )
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        List<Character> liste = new ArrayList<>();
        int ind = 0;
        while(ind < chaine.length()){
            char c = chaine.charAt(ind);
            if(c=='(' || c=='['){ // si le caractères est un ( ou un [ on l'ajoute à la fin d'une liste
                liste.add(c);
            }
            else{
                if(c==')'){
                    if(liste.size() > 0 && liste.get(liste.size()-1).equals('(')){ // ainsi comme le caractère est un ) on regarde si la liste n'est pas vide pour éviter une ")" de trop et on vérifie que le derier élément de la liste soit bien un "(" car sinon cela signifie qu'il y a un problème du type ([)]
                        liste.remove(liste.size()-1);
                    }
                    else{
                        return false;
                    }
                }
                else{
                    if(c==']'){
                        if(liste.size() > 0 && liste.get(liste.size()-1).equals('[')){ // la même chose qu'avec les parenthèse mais avec les crochets
                            liste.remove(liste.size()-1);
                        }
                        else{
                            return false;
                        }

                    }
                }
            }
            ind += 1;
        }
        return liste.size() == 0;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if(liste.size() != 0){
            int min = 0;
            int max = liste.size()-1; // on créer des bornes qui sont initialisé entre le début et la fin de la liste
            int ind = (int)(min+max)/2;
            while(max - min >1){// pour sortir de la boucle il faut que la borne maximale et minamale soit "collées"
                if(liste.get(ind).compareTo(valeur) < 0){// si la valeur est plus grande que l'élément étudié alors la borne minale se place au niveau de l'élément étudié
                    min = ind;
                    ind = (int)(min+max)/2;
                }
                else{
                    if(liste.get(ind).compareTo(valeur) > 0){ // sinon c'est la borne maximal qui se place au niveau de l'élément étudié
                        max = ind;
                        ind = (int)(min+max)/2;
                    }
                    else{
                        return true; // nous arrivons ici lorsque l'élément étudié n'est ni supérieur , ni inférieur , ce qui signifie que l'élément étudié est égalà la valeur souhaité
                    }
                }
            }
            if(liste.get(min).equals(valeur) || liste.get(max).equals(valeur)){ // on vérifie quand même que les bornes ne soient pas égales à la valeur recherché
                return true;
            }
        }
        return false;
    }



}
